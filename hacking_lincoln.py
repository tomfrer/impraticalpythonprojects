import math

# get encrypted text message

encrypted_message = 'THIS OFF DETAINED ASCERTAIN WAYLAND ' + \
                'CORRESPONDENTS OF AT WHY AND IF FILLS ' + \
                'IT YOU GET THEY NEPTUNE THE TRIBUNE ' + \
                'PLEASE ARE THEM CAN UP'

# encrypted_message = 'REST TRANSPORT YOU GOODWIN VILLAGE ROANOKE ' + \
#                     'WITH ARE YOUR IS JUST SUPPLIES FREE SNOW ' + \
#                     'HEADING TO GONE TO SOUTH FILLER'

encrypted_list = encrypted_message.split(' ')

# print(f'The message contains {len(encrypted_list)} words\n')
#
# # guess number of rows and columns
# columns, rows = (int(x) for x in input('Enter number of columns, rows: ').replace(',', ' ').split())
# # columns = 4
# # rows = 5
#
# # guess cipher key
# cipher_key = input('Enter cipher key: ')
# # cipher_key = '-1 2 -3 4'
# cipher_key_list = (int(x) for x in cipher_key.replace(',', ' ').split())

# -------------------------- End of user input -------------------------------------


def get_grid_combinations(message_length):
    i = 2
    combos = []
    while i <= math.ceil(message_length / 2):
        if message_length % i == 0:
            combo = [i, int(message_length / i)]
            combos.append(combo)
        i += 1
    return combos


def get_corner_combinations(message_columns):
    combos = []
    # starting at first column, not reversed
    combo = []
    for i in range(int(message_columns)):
        index_number = i + 1  # not zero based
        multiplier = int(math.pow(-1, i))
        combo.append(index_number * multiplier)
    combos.append(combo)
    # starting at first column, reversed
    combo = []
    for i in range(int(message_columns)):
        index_number = i + 1  # not zero based
        multiplier = int(math.pow(-1, i+1))
        combo.append(index_number * multiplier)
    combos.append(combo)
    # starting at last column, not reversed
    combo = []
    for i in range(int(message_columns), 0, -1):
        multiplier = int(math.pow(-1, i))
        combo.append(i * multiplier)
    combos.append(combo)
    # starting at last column, reversed
    combo = []
    for i in range(int(message_columns), 0, -1):
        multiplier = int(math.pow(-1, i+1))
        combo.append(i * multiplier)
    combos.append(combo)
    return combos


grid_combinations = get_grid_combinations(len(encrypted_list))

for grid_combination in grid_combinations:
    columns = grid_combination[0]
    rows = grid_combination[1]
    key_combinations = get_corner_combinations(columns)
    for key_combination in key_combinations:
        print('\n--------------------------------------------------------------------------------------')
        print(grid_combination, key_combination)
        # encryption turned columns into rows, so read sequential words into columns
        encrypted_list_by_columns = []
        el_iter = iter(encrypted_list)
        for c in range(columns):
            col_list = []
            for r in range(rows):
                col_list.append(next(el_iter))
            encrypted_list_by_columns.append(col_list)

        # some of the columns were reversed by applying the key,
        # so un-reverse them
        plain_text_list = []
        for k in key_combination:
            if k > 0:  # column is top to bottom
                for r in range(rows):
                    plain_text_list.append(encrypted_list_by_columns[abs(k) - 1][r])
            else:  # column is bottom to top
                for r in range(rows - 1, -1, -1):
                    plain_text_list.append(encrypted_list_by_columns[abs(k) - 1][r])

        # pull every 'rowth' element (essentially turn columns back to rows
        plain_text = ''
        for r in range(rows):
            for c in range(columns):
                plain_text += plain_text_list[r + c * rows] + ' '

        print(f'Decoded Message:\n{plain_text}')

