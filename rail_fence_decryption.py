encrypted_text = input('Enter encrypted text: ').replace(' ', '').upper()

odd_char = len(encrypted_text) % 2

first_line = encrypted_text[0:odd_char+int(len(encrypted_text)/2)]
second_line = encrypted_text[odd_char+int(len(encrypted_text)/2):]

interwoven = ''
for i in range(len(first_line)):
    interwoven += first_line[i]
    if i < len(second_line):
        interwoven += second_line[i]

print(interwoven)
