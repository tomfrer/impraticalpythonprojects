import json
import time

"""
    the code below assumes you have an anagram json file

    the json file can be rebuilt using the code below (assuming you have a 
    file containing valid words)

-------------------------------------------------------------------------------------------------
    
    import re

    def remove_extra_chars(word):
        return re.sub(r'\W+', '', word)

    def add_to_dictionary(dictionary, key, value):
        if key in dictionary:
            current_list = dictionary[key]
            current_list.append(value)
            dictionary[key] = current_list
        else:
            dictionary[key] = [value]
        return dictionary

    try:
        with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
            word_list = word_file.read().strip().split('\n')
        word_list = [remove_extra_chars(a) for a in word_list]

        for word in word_list:
            if len(word) > 1:
                sorted_word = ''.join(sorted(word))
                anagram_dict = add_to_dictionary(anagram_dict, sorted_word, word)
    
        with open('C:\\Users\\tomfr\\useful_data\\anagrams.json', "w") as outfile:
            json.dump(anagram_dict, outfile)

    except IOError as e:
        print(f'IOError: {str(e)}')

"""

print("Enter a word and for which you want to find anagrams:")
word_in = input().lower()
sorted_word_in = ''.join(sorted(word_in))

start_time = time.time()

try:

    with open('C:\\Users\\tomfr\\useful_data\\anagrams.json') as json_file:
        anagram_dict = json.load(json_file)

    print(f'\nAnagrams for {word_in}:')
    print(*anagram_dict[sorted_word_in], sep='\n')

except IOError as e:
    print(f'IOError: {str(e)}')

print(f'\nelapsed time = {time.time() - start_time}')
