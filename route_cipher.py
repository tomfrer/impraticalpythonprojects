# the cipher text to decode
cipher_text = '16 12 8 4 0 1 5 9 13 17 18 14 10 6 2 3 7 11 15 19'
print(cipher_text)
cipher_list = cipher_text.split(' ')

# get user input
columns, rows = (int(x) for x in input('Enter number of columns, rows: ').replace(',', ' ').split())
cipher_key = input('Enter cipher key: ')
cipher_key_list = (int(x) for x in cipher_key.replace(',', ' ').split())

# convert cipher_list into list of lists (each sub list = a column)
cipher_list_by_columns = []
cl_iter = iter(cipher_list)
for c in range(columns):
    col_list = []
    for r in range(rows):
        col_list.append(next(cl_iter))
    cipher_list_by_columns.append(col_list)

print(cipher_list_by_columns)

plain_text_list = []
for k in cipher_key_list:
    if k > 0:  # column is top to bottom
        for r in range(rows):
            plain_text_list.append(cipher_list_by_columns[abs(k)-1][r])
    else:  # column is bottom to top
        for r in range(rows-1, -1, -1):
            plain_text_list.append(cipher_list_by_columns[abs(k)-1][r])

# the resulting plain_text_list will be in column order
# i.e. [c1r1, c1r2, c1r3, c1r4, c1r5, c1r1, c2r2 . . . .c4r4, c4r5]
# need to put it on row order
# i.e. [c1r1, c2r1, c3r1, c4r1, c1r2, c2r2 . . . c3r5, c4r5]
plain_text = ''
for r in range(rows):
    for c in range(columns):
        plain_text += plain_text_list[r + c * 5] + ' '

print(plain_text_list)
print(plain_text)
