import re
import time


def remove_extra_chars(word):
    return re.sub(r'\W+', '', word)


# palindromes = []
# try:
#     with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
#         for word in word_file:
#             word = remove_extra_chars(word.strip())
#             if word == word[::-1]:
#                 palindromes.append(word)
#     print(f'\n{len(palindromes)} palindromes found!\n')
#     print(*palindromes, sep='\n')
# except IOError as e:
#     print(f'IOError: {str(e)}')

start_time = time.time()
palingrams = []
try:
    with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
        word_list = word_file.read().strip().split('\n')

    word_list = [remove_extra_chars(a) for a in word_list]
    words = set(word_list)
    for word in words:
        if len(word) > 1:
            for i in range(len(word)):
                if word[:i][::-1] in words:
                    if word[i:] == word[i:][::-1]:
                        palingrams.append(word + ' ' + word[:i][::-1])
                if word[i:][::-1] in words:
                    if word[:i] == word[:1][::-1]:
                        palingrams.append(word[i:][::-1] + ' ' + word)

    sorted_palingrams = sorted(palingrams)
    print()
    print(*sorted_palingrams, sep='\n')
    print(f'\n{len(palingrams)} palingrams found!')
except IOError as e:
    print(f'IOError: {str(e)}')

print(f'\nelapsed time = {time.time() - start_time}')
