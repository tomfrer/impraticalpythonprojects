import re
import random


def remove_extra_chars(word):
    return re.sub(r'\W+', '', word)


def get_word_list():
    word_list = []
    try:
        with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
            word_list = word_file.read().strip().split('\n')
        word_list = [remove_extra_chars(a) for a in word_list]
    except IOError as e:
        print(f'IOError: {str(e)}')
    return word_list


def find_random_matching_word(word_list, initial_char, char, position):
    matches = []
    random.seed()
    for word in word_list:
        if word[0:1] == initial_char and len(word) > 5:
            search_string = '^.{' + str(position - 1) + '}' + char + '.*$'
            found = re.search(search_string, word)
            if found:
                matches.append(word)
    random_index = random.randint(0, len(matches)-1)
    return matches[random_index]


def main():
    # hidden_text = 'panelateastendofchapelslides'
    hidden_text = 'mist'
    word_list = get_word_list()
    # char_to_find = 'a'
    initial_char = 'w'
    char_position = 5
    new_word_list = []
    for char in hidden_text:
        new_word_list.append(find_random_matching_word(word_list, initial_char, char, char_position-1))

    for word in new_word_list:
        print(word)


if __name__ == '__main__':
    main()






