import re

+
def remove_extra_chars(word):
    return re.sub(r'\W+', '', word)


with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
    word_list = word_file.read().strip().split('\n')

word_list = [remove_extra_chars(a) for a in word_list]

with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf_clean.txt', 'w') as clean_file:
    for word in word_list:
        clean_file.write(word + '\n')

print('done')
