import re


def get_message_text(file_name):
    # read the file into a string
    with open(file_name, 'r') as file:
        message_text = file.read().replace('\n', '')
    return message_text


def break_text_at_punctuation(message_text):
    chunks = re.split(r'[;:\.\,\']', message_text)
    # throw away the first chunk
    chunks.pop(0)
    return chunks


def extract_plain_text(chunk_list, letters_after):
    plain_text = ''
    for chunk in chunk_list:
        if len(chunk) >= 3:
            plain_text += chunk.replace(' ', '')[letters_after - 1:letters_after]
    return plain_text


def main():
    message_text = get_message_text('trevanion.txt')
    message_chunks = break_text_at_punctuation(message_text)
    # set the number of characters after a punctuation
    letters_after = 3
    plain_text = extract_plain_text(message_chunks, letters_after)
    print(plain_text)

if __name__ == '__main__':
    main()