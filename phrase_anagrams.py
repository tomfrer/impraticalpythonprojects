import re
import textwrap
from collections import Counter


def remove_extra_chars(word):
    return re.sub(r'\W+', '', word)


def word_is_in_phrase(word_to_check, phrase):
    word_count = Counter(word_to_check)
    phrase_count = Counter(phrase)
    result = True
    for k, v in word_count.items():
        if word_count[k] > phrase_count[k]:
            result = False
    return result


def pull_word_from_word(outer_word, inner_word):
    for c in inner_word:
        outer_word = outer_word.replace(c, '', 1).replace(' ', '')
    return outer_word


try:
    with open('C:\\Users\\tomfr\\useful_data\\12dicts\\American\\2of12inf.txt') as word_file:
        word_list = word_file.read().strip().split('\n')
    word_list = [remove_extra_chars(a) for a in word_list]
    words = set(word_list)
except IOError as e:
    print(f'IOError: {str(e)}')

print("Enter a phrase or name:")
phrase_in = input().lower().replace(' ', '')
search_phrase = phrase_in
# phrase_in = 'clint eastwood'
phrase_out = ''

while len(search_phrase) > 0:
    possibilities = []
    for word in words:
        if word_is_in_phrase(word, search_phrase):
            possibilities.append(word)
    possibilities = sorted(possibilities)

    if len(possibilities) > 0:
        print(f'\nwords found within: {search_phrase}\n')
        possibilities_string = ''
        for possible in possibilities:
            possibilities_string += possible + ', '
        possible_list = textwrap.wrap(possibilities_string[:len(possibilities_string)-2], width=80)
        print(*possible_list, sep='\n')

        print('\nSelect a word to pull from the list (0 to restart or ENTER to quit):')
        word_to_pull = input().strip().lower().replace(' ', '')
        if word_to_pull == '':
            search_phrase = ''
        elif word_to_pull == '0':
            search_phrase = phrase_in
            phrase_out = ''
        else:
            if word_to_pull in possibilities:
                search_phrase = pull_word_from_word(search_phrase, word_to_pull)
                phrase_out += word_to_pull + ' '
            else:
                print('\nSorry that word is not one of the possibilities!')
    else:
        print(f'\nNo viable words left! ({search_phrase})')
        search_phrase = ''
        phrase_out = ''

if phrase_out != '':
    print(f'Your phrase anagram is: {phrase_out}')
else:
    print('No phrase anagram was created :(')
