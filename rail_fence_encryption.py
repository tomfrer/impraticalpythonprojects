plain_text = input('Enter message to be encrypted: ').replace(' ', '').upper()

first_line = ''
second_line = ''
for i in range(len(plain_text)):
    if i % 2 == 0:
        first_line += plain_text[i]
    else:
        second_line += plain_text[i]

whole_line = first_line + second_line

five_letter_groups = ''
for i in range(len(whole_line)):
    five_letter_groups += whole_line[i]
    if (i + 1) % 5 == 0:
        five_letter_groups += ' '

print(f'encrypted text: {five_letter_groups}')
